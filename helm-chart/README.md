Instantiation with Helm
---

The duke-shibboleth-container helm chart will instantiate
apache and shibboleth containers according to [These Instructions](https://openshift-docs.cloud.duke.edu/user-guide/dukeShibboleth/).

#### Usage

The chart requires the following values to work:

entityID: This is used as the primary CN in the certificate, and
  registered with the Shibboleth Site Management System with
  the certificate. It is used to generate the shibboleth2.xml,
  and the Route.spec.host.
tls.crt: The certificate used to register the entityID with the
  Shibboleth Site Management System. This can be set to the contents of a file
    using the --set-file argument to helm.
tls.key: The key to the cert. This can be set to the contents of a file
  using the --set-file argument to helm.
imageRegistry: path to the project gitlab repo container registry.
  The GitlabCI Runner supplies this in the CI_REGISTRY_IMAGE environment
  variable.
registry: This is a hash, with the following values:
  root: path to the gitlab registry. The GitlabCI Runner supplies this
    in the CI_REGISTRY environment variable.
  secret: This is a hash, with the following values:
    username: username to use to pull the image from the Gitlab Project
      registry. If you create a gitlab-deploy-token deployment token in
      the project, this is supplied by the GitlabCI Runner in the
    CI_DEPLOY_USER environment variable.
      password: Password for the user used to pull the image
      from the Gitlab Project Registry. If you create a
      gitlab-deploy-token deployment token in the project,
      this is supplied in the CI_DEPLOY_PASSWORD environment variable.

The chart allows the following values to be supplied as well:
applicationConfiguration: This can be set to the contents of a file
  using the --set-file argument to helm. If present,
  Its contents are mounted into the httpd container as /etc/httpd/
  conf.d/application.conf, which will be loaded into the overall httpd
  configuration.
shibboleth.attributeMap: This can be set to the contents of a file
  using the --set-file argument to helm. If present, its contents
  are loaded into the shibboleth container as /etc/shibboleth/attribute-map.xml.
git_commit: added as a label in the deployment.

```bash
helm upgrade --force --recreate-pods --debug \
--set registry.root=${CI_REGISTRY} \
--set registry.secret.username=${CI_DEPLOY_USER} \
--set registry.secret.password="${CI_DEPLOY_PASSWORD}" \
--set entityID=${ENTITY_ID} \
--set-file tls.crt="${ENTITY_ID}.crt" \
--set-file tls.key="${ENTITY_ID}.key" \
--set imageRegistry="${CI_REGISTRY_IMAGE}" \
${extraArgs} \
--wait \
--install ${CI_ENVIRONMENT_NAME} helm-chart/duke-shibboleth-container
```

To add an attribute map, and an application.conf file, do the following instead
```bash
extraArgs='--set-file applicationConfiguration=application.conf-example --set-file shibboleth.attributeMap=attribute-map.xml-grouper-example'
helm upgrade --force --recreate-pods --debug \
--set registry.root=${CI_REGISTRY} \
--set registry.secret.username=${CI_DEPLOY_USER} \
--set registry.secret.password="${CI_DEPLOY_PASSWORD}" \
--set entityID=${ENTITY_ID} \
--set-file tls.crt="${ENTITY_ID}.crt" \
--set-file tls.key="${ENTITY_ID}.key" \
--set imageRegistry="${CI_REGISTRY_IMAGE}" \
${extraArgs} \
--wait \
--install ${CI_ENVIRONMENT_NAME} helm-chart/duke-shibboleth-container
```


#### Useful Helm Commands

It is easiest (and cross Openshift/Kubernetes compatible) to run a
local instance of tiller for helm to use (at least until we upgrade
to helm 3). [Tillerless Helm](https://rimusz.net/tillerless-helm/)

You must be have your oc or kubectl cli logged in to the cluster
you wish to deploy to for this to work.

```bash
export TILLER_NAMESPACE=$PROJECT_NAMESPACE
export HELM_HOST=localhost:44134
tiller --storage=secret &
export TILLER_PID=$!
sleep 1
kill -0 ${TILLER_PID}
if [ $? -gt 0 ]
then
  raise "tiller not running!"
fi
helm init --client-only
helm repo update
```

You can also list which deployments are registered in your locally running tiller:
```
helm list
```

This can be useful to test if your session has expired. If it has, you
will likely see errors saying your user does not have the ability to
list things, e.g. the same errors you would get if you ran oc get all.

If you want to delete EVERYTHING for a deployment, use helm delete (the -purge argument removes information stored by the locally running tiller). To delete this helm chart, use the same CI_ENVIRONMENT_NAME that you used in the upgrade above:
```
helm delete --purge ${CI_ENVIRONMENT_NAME}
```

Helm allows you to lint a chart
```
helm lint helm-chart/duke-shibboleth-container
```

You can also print out all of the things that would be installed
for the chart using helm template (with or without the same --set and -
-set-file arguments used in helm upgrade). The only difference is that
you need to supply a name to the template which is required to upgrade
the template.

```bash
helm template \
--set registry.root=${CI_REGISTRY} \
--set registry.secret.username=${CI_DEPLOY_USER} \
--set registry.secret.password="${CI_DEPLOY_PASSWORD}" \
--set entityID=${ENTITY_ID} \
--set-file tls.crt="${ENTITY_ID}.crt" \
--set-file tls.key="${ENTITY_ID}.key" \
--set imageRegistry="${CI_REGISTRY_IMAGE}" \
${extraArgs} \
-n ${CI_ENVIRONMENT_NAME} helm-chart/duke-shibboleth-container
```

Unfortunately, there is no way to print out the template of just one
item.

You should kill the locally running tiller after you are finished
with it.

```bash
kill $TILLER_PID.
```
